import javax.swing.JOptionPane;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Stack;
import java.util.Vector;
import java.util.List;

public class Clase01_Ej2 {

	public static void main(String[] args) {
		
		//Tiempos de creaci�n de cada LIST:
		System.out.println("Tiempo de creaci�n de ArrayList: ");
		List<Cliente> listClientesAL = new ArrayList<Cliente>();
		calculaTiempo(listClientesAL);
		
		System.out.println("Tiempo de creaci�n de LinkedList: ");
		List<Cliente>listClientesLL = new LinkedList<Cliente>();
		calculaTiempo(listClientesLL);
		
		System.out.println("Tiempo de creaci�n de Vector: ");
		List<Cliente>listClientesV = new Vector<Cliente>();
		calculaTiempo(listClientesV);
		
		System.out.println("Tiempo de creaci�n de Stack: ");
		List<Cliente>listClientesS = new Stack<Cliente>();
		calculaTiempo(listClientesS);
		
		//Tiempos de lectura de cada LIST:
		System.out.println("Tiempo de lectura de ArrayList: ");
		tiempoDeLectura(listClientesAL);
		
		System.out.println("Tiempo de lectura de LinkedList: ");
		tiempoDeLectura(listClientesLL);
		
		System.out.println("Tiempo de lectura de Vector: ");
		tiempoDeLectura(listClientesV);
		
		System.out.println("Tiempo de lectura de Stack: ");
		tiempoDeLectura(listClientesS);
		
		//Ejemplo de iterador
		List<Cliente> listCliente = new ArrayList<Cliente>();
		for (long i = 0; i < 5; i++) {//Agrego elementos a la lista
			listCliente.add(new Cliente(i,i,"Cliente"+i));
		}

		Iterator<Cliente> iterador = listCliente.iterator();
		//Creo el iterador sobre la lista con WHILE
		System.out.println("Iterator con WHILE");
		while (iterador.hasNext()) { //Condici�n booleana
			System.out.println(iterador.next());
		}
		//Otra forma con un FOR (Autocompletado)
		System.out.println("Iterator con FOR");
		for (Iterator iterator = listCliente.iterator(); iterator.hasNext();) {
			Cliente cliente = (Cliente) iterator.next();
			System.out.println(cliente);//Solo agreg� esta linea
		}
		
		//Scanner:
		//Recordar importar java.util.Scenner;
		//DATOS NUM�RICOS:
		System.out.println("Ingrese un numero usando scanner :");
		Scanner lector= new Scanner(System.in);
		int num =Integer.parseInt(lector.nextLine()); //CASTEO a INT!
		//int num2 = lector.nextInt(); Otra opci�n con nextInt.
		//Double num3 = lector.nextDouble(); Otra opci�n con nextdouble.
		System.out.println("la n�mero con scanner es " + num );
		//DATOS TIPO TEXTO:
		System.out.println("Ingrese otro texto usando scanner : ");
		String x =lector.nextLine();
		System.out.println("otro texto con scanner es " + x );
		
		//JOptionPane:
		// Recordar importar javax.swing.JOptionPane!
		String textoIngresado=JOptionPane.showInputDialog
				("Ingrese texto utilizando JOptionPane");
		//System.out.println("Texto ingresado:"+textoIngresado);
		//Si lo quiero mostrar en otro JOptionPane:
		JOptionPane.showMessageDialog(null, "El texto es: "+textoIngresado);
		
		
		//BufferedReader:
		// Recordar importar java.io!
				String texto=""; //Siempre inicializar los String!
				System.out.println("ingrese texto por "
						+ "pantalla con BufferedReader");
				BufferedReader in = new BufferedReader
						(new InputStreamReader(System.in));
				// Cargo los datos por consola.
				try {
					texto = in.readLine(); 
					// Asigna a "texto" el String ingresado por consola.
				} catch (Exception e) {
					System.out.println("Error!");
				}
				System.out.println("El texto ingresado con "
						+ "BufferedReader es: "+texto);
		
		/*
		 * int i = 10; if (i > 10) { System.out.println("i es mayor a 10"); } else if (i
		 * < 10) {
		 * 
		 * System.out.println("i es menor igual a 10"); } else {
		 * System.out.println("i es igual a 10");
		 * 
		 * }
		 */

		int[] arrayDeEnteros = new int[5];

		arrayDeEnteros[0] = 0;
		arrayDeEnteros[1] = 1;
		arrayDeEnteros[2] = 2;
		arrayDeEnteros[3] = 3;
		arrayDeEnteros[4] = 4;

		for (int numeros : arrayDeEnteros) {
			System.out.println(numeros);
		}

		String texto1 = "Hola";
		String texto2 = "Chau";

		if (texto1.equalsIgnoreCase(texto2)) {
			System.out.println("Los textos son iguales!");
		} else {
			System.out.println("Los textos son diferentes!");
		}

		System.out.println("Mi array tiene un total de " 
		+ arrayDeEnteros.length + " posiciones");

		for (int i = 0; i < arrayDeEnteros.length; i++) {
			System.out.println("El valor de la posici�n " 
		+ i + " es: " + arrayDeEnteros[i]);
		}

		float[][] matrizDeFloat = new float[15][5];
		int contador = 0;
		// Terminar ver clase 1:30

		for (int i = 0; i < matrizDeFloat.length; i++) {
			for (int j = 0; j < matrizDeFloat[i].length; j++) {

				System.out.println("El valor de la posicion" + i + " x " + j + " es " + matrizDeFloat[i][j]);
			}

		}
		
		int numero =5;
		do {
			System.out.println("El n�mero es: "+numero);
			numero--;
		}while(numero >1);
		
		int opcion = 2;
		switch (opcion) {
		case 1: {
			System.out.println("Seleccion� la opci�n N� 1");
		}
			break;
		case 2: {
			System.out.println("Seleccion� la opci�n N� 2");
		}
			break;
		default: //Se ejecuta si la opci�n es distinta de los case.
			System.out.println("Opci�n no v�lida");
		}

	}
	
	private static void tiempoDeLectura(List<Cliente> listClientes) {
		// TODO Auto-generated method stub
		long inicio = System.currentTimeMillis();
		for (Iterator iterator = listClientes.iterator(); iterator.hasNext();) {
			Cliente cliente = (Cliente) iterator.next();
		}
		long fin=System.currentTimeMillis();
		long tiempo= (fin-inicio);
		System.out.println("Tiempo de lectura: "+tiempo+" mili-segundos");
		
	}

	private static void calculaTiempo(List<Cliente>listaClientes) {
		long inicio = System.currentTimeMillis();
		for(long i=0;i<1000000;i++) {
			listaClientes.add(new Cliente(i,i,"Nombre"+i)); //Cargo la lista.
		}
		long fin = System.currentTimeMillis();
		long tiempo = (fin-inicio);
		System.out.println(tiempo+" mili-segundos");
	}

}
