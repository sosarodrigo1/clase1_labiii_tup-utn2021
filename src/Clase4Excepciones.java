import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JOptionPane;

public class Clase4Excepciones {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i = 10;
		ArrayList<Integer> arlI = new ArrayList<Integer>();
		while (i > 0) {
			i--;
			try {
				addIngreso1(arlI);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "No num�rico" + e.getMessage());
				i=0;
			}
		}
		muestroLista(arlI);
	}

	private static void muestroLista(ArrayList<Integer> arlI) {
		for (Iterator<Integer> iterador = arlI.iterator(); iterador.hasNext();) {
			Integer valorInt = (Integer) iterador.next();
			System.out.println("Valor: " + valorInt);
		}
	}

	private static void addIngreso1(ArrayList<Integer> arlI) throws Exception {
		// TODO Auto-generated method stub
		arlI.add(Integer.parseInt(JOptionPane.showInputDialog("Ingrese un n�mero")));
	}
}
