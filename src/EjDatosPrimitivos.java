
public class EjDatosPrimitivos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		boolean bo = true;
		int i = 2;
		short s = 3;
		char letra = 'a';
		byte b = 2;
		long l = 65854;
		float f = 32.6f;
		double d = -12.45;
		String texto = "Hola Mundo!";
		//SHIFT+CTRL+F (Arregla el formato)
		//CTRL+SPACE ayuda autompletar. Ejemplo...sysout + CTRL+SPACIO autocompleta el codigo
		
		System.out.println("El valor de la varible booleana es:"+bo);
		System.out.println("int: "+i);
		System.out.println("short: "+s);
		System.out.println("char: "+letra);
		System.out.println("byte: "+l);
		System.out.println("float: "+f);
		System.out.println("double: "+d);
		System.out.println("int: "+texto);
		
		//Clases, objetos ( no son primitivos como los anteriores
		Short s2=3;
		Integer i2=2;
		
		System.out.println("Short: "+s2);
		
		System.out.println("El maximo valor de un int es: "+Integer.MAX_VALUE);
		System.out.println("El minimo valor de un byte es: "+Byte.MIN_VALUE);
		System.out.println("El minimo valor de un double es: "+Double.MIN_VALUE);
		
		
		//TODO int byte y double. Si escribimos TODO lo marca en el scroll del codigo.
		
		
	

	}

}
