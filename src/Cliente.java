
public class Cliente {
	private long iD;
	private long dNI;
	private String nombre;
	
	public Cliente(long iD, long dNI, String nombre) {
		super();
		this.iD = iD;
		this.dNI = dNI;
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Cliente "+iD+": "+nombre+" - DNI: "+dNI+".";
	}
	

}
